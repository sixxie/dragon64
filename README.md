# Dragon 64 schematic

... and PCB layout, in [KiCad](https://www.kicad.org/).  Now requires Kicad 7.

I did use [Freerouting](https://github.com/freerouting/freerouting) to get a
starting point for the layout, but I've rerun almost all of it since then.

Changes from the original:

 * The 240° 5-pin DIN sockets used for joysticks appear to be a bit harder to
   find now.  I've replaced them with (compatible) 6-pin sockets.
 * The footprint for the 7-pin DIN serial connector is incompatible (2-3-2
   instead of 3-4).  This reflects what I've found to be available today.
 * Two more inductors and capacitors have been added as optional components to
   enable the second firebutton on CoCo 3 compatible joysticks.
 * The mounting holes for the edge connector have been made into ovals; in theory,
   both the original edge connectors, and those made by EDAC should fit, if you
   can find one.

I've not yet been able to test the printer & serial ports.

## Issue 4

"Issue 4" worked, with caveats:

 * In the original schematic scans, you cannot see the MS# on the VDG is
   supposed to be connected through to FS#, and this made it through to my
   capture.  A bodge wire is required.
 * The time constant for the colourburst switch seems a bit long.  Replacing
   R72 with 20K gives a better result.
 * The mounting holes are slightly offset.  I'm able to screw it into a real
   case, but wouldn't also be able to attach the cartridge port cover.

## Issue 5

The problems mentioned are now theoretically fixed in Issue 5, though I've not
yet had any of those boards made.

I think the PCB now has reasonable power distribution, and where possible, I've
tried to route such that the decoupling capacitor positions make sense.
Crystal tracks should be reasonably well separated from anything else, and
apart from a tiny skirt around the outside of the ACIA crystal housing, no
tracks pass underneath them either.

## Issue 6

Many more tweaks to beef up the power and ground paths.  Solder jumpers replace
existing links (which need connecting on assembly), and two more added:

 * Tie MS# either HIGH (required if HCT logic is used for IC13) or to FS# (IC13
   must be LS logic, but this allows the use of a typical lowercase board
   design).
 * PIA HSync selectable between NHS# (original Dragon 64) and CHS# (combined
   signal, as per Dragon 32).

Furthermore, the combined signal is always routed to the SAM, for better DRAM
refresh behaviour.

## Acknowledgements

Thanks to Richard Harding for hosting high quality copies of the original
drawings - doing this from the old scans would have been nigh on impossible.

 * [Original drawings](http://dragondata.co.uk/tech/circuit-diag/index.html)

Thanks also to Stewart Orchard, who has done a similar capture of the Dragon's
PSU board.  I also nabbed his 3D model of the DIN socket to represent all the
similar ports in this project (you'll notice they're the wrong shape for the
joystick ports, and none of them have more than three pins).

 * [DragonISS4PSU](https://gitlab.com/sorchard001/dragoniss4psu)

Thanks to Julian Brown for providing edge connectors and a reset switch,
enabling me to build a complete board.  These are both quite hard to find.
Julian is also pursuing a similar project to make a Dragon 32 replica.
